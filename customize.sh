. $MODPATH/ohmyfont.sh

### INSTALLATION ###

ui_print '- Installing'

ui_print '+ Prepare'
prep

ui_print '+ Configure'
config

ui_print '+ Google Sans'
GSRR=GoogleSansRounded-$Re$X
GSF=$FONTS/$GSR
ROUNDED=`valof ROUNDED`
HEBREW=`valof HEBREW`

${ROUNDED:=false} && {
    GSF=$FONTS/$GSRR
    ui_print '  Rounded'
}

[ "$HEBREW" = false ] && {
    ui_print '  Removing Hebrew (15s)...'
    pyftsubset $GSF \
        --unicodes=u0-58f,u600-ffffff \
        --passthrough-tables \
        --output-file=$FONTS/$SS || \
        abort
}

mv -n $GSF $FONTS/$SS
install_font

src

ui_print '+ Rom'
rom
fontspoof
finish
